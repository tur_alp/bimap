﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BIProject.Models;

namespace BIProject.Controllers
{
    public class DataSourcesController : ApiController
    {
        private BIMapContext db = new BIMapContext();

        // GET: api/DataSources
        public IQueryable<DataSource> GetDataSources()
        {
            return db.DataSources;
        }

        // GET: api/DataSources/5
        [ResponseType(typeof(DataSource))]
        public async Task<IHttpActionResult> GetDataSource(int id)
        {
            DataSource dataSource = await db.DataSources.FindAsync(id);
            if (dataSource == null)
            {
                return NotFound();
            }

            return Ok(dataSource);
        }

        // PUT: api/DataSources/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDataSource(int id, DataSource dataSource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dataSource.Id)
            {
                return BadRequest();
            }

            db.Entry(dataSource).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DataSourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DataSources
        [ResponseType(typeof(DataSource))]
        public async Task<IHttpActionResult> PostDataSource(DataSource dataSource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DataSources.Add(dataSource);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = dataSource.Id }, dataSource);
        }

        // DELETE: api/DataSources/5
        [ResponseType(typeof(DataSource))]
        public async Task<IHttpActionResult> DeleteDataSource(int id)
        {
            DataSource dataSource = await db.DataSources.FindAsync(id);
            if (dataSource == null)
            {
                return NotFound();
            }

            db.DataSources.Remove(dataSource);
            await db.SaveChangesAsync();

            return Ok(dataSource);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DataSourceExists(int id)
        {
            return db.DataSources.Count(e => e.Id == id) > 0;
        }
    }
}