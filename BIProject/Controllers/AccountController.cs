﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using BIProject.Models;


namespace BIProject.Controllers
{
    public class AccountController : Controller
    {
        private UserManagerApplication UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<UserManagerApplication>();
            }
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new Models.User { FirstName = model.FirstName, LastName = model.LastName, UserName = model.Username };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                    return RedirectToAction("Login", "Account");
                else
                    foreach (string error in result.Errors)
                        ModelState.AddModelError("", error);
            }
            return View(model);
        }
    }
}