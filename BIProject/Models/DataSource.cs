﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BIProject.Models
{
    public class DataSource
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [MaxLength(10)]
        public string Type { get; set; }
        [Required]
        public string Content { get; set; }
        
    }
}