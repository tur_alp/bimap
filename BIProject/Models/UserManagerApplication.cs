﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BIProject.Models
{
    public class UserManagerApplication : UserManager<User>
    {
        public UserManagerApplication(IUserStore<User> store) : base(store)
        {

        }

        public static UserManagerApplication Create(IdentityFactoryOptions<UserManagerApplication> options, IOwinContext context)
        {
            BIMapContext db = context.Get<BIMapContext>();
            UserManagerApplication manager = new UserManagerApplication(new UserStore<User>(db));
            return manager;
        }
    }
}