namespace BIProject.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<BIProject.Models.BIMapContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BIProject.Models.BIMapContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.DataSources.AddOrUpdate(x => x.Id,
                new DataSource() { Id = 1, Name = "example", Description = "test", Type = "json", Content = "[{'name': 'Rafael Bank', 'lon': '50.468250', 'lat': '30.478204'},{'name': 'Privat Bank','lon': '50.468798','lat': '30.478998'}]" });
        }
    }
}
